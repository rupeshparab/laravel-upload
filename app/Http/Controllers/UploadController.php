<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use Request;
use Session;
class UploadController extends Controller 
{
    public function upload() 
    {
      $file = array('image' => Input::file('image'));
      $rules = array('image' => 'required',);
      $validator = Validator::make($file, $rules);
      if ($validator->fails()) 
      {
        return Redirect::to('/')->withInput()->withErrors($validator);
      }
      else {
        if (Input::file('image')->isValid()) 
        {
          $destinationPath = 'uploads';
          $oname = Input::file('image')->getClientOriginalName();
          $extension = Input::file('image')->getClientOriginalExtension(); 
          $fileName = rand(11111,99999).'-'.$oname.'.'.$extension;
          Input::file('image')->move($destinationPath, $fileName); 
          Session::flash('success', 'Upload successfully'); 
          return Redirect::to('/');
        }
        else 
        {
          Session::flash('error', 'uploaded file is not valid');
          return Redirect::to('/');
        }
      }
    }
}